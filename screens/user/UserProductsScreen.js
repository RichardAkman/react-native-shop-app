import React, {useCallback, useEffect, useState} from 'react';
import {Button, FlatList, Alert, View, ActivityIndicator, StyleSheet} from "react-native";
import ProductItem from "../../components/shop/ProductItem";
import {useSelector, useDispatch} from "react-redux";
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import CustomHeaderButton from "../../components/UI/HeaderButton";
import Colors from "../../constants/Colors";
import {deleteProduct, getProducts} from "../../redux/actions/products";
import DefaultText from "../../components/UI/DefaultText";

const UserProductsScreen = props => {
    const [loading, setLoading] = useState(false)
    const [refreshing, setRefreshing] = useState(false);
    const [error, setError] = useState('');
    const products = useSelector(state => state.products.myProducts);
    const dispatch = useDispatch();

    // Setup product loading from the server
    const loadProducts = useCallback(async () => {
        setError('');
        setRefreshing(true)
        try {
            await dispatch(getProducts())
        } catch (err) {
            setError(err.message);
        }
        setRefreshing(false)
    }, [dispatch, setLoading, setError, setRefreshing])

    const editItemHandler = (id) => {
        props.navigation.navigate('EditProduct', {
            productID: id,
        })
    }

    useEffect(() => {
        if (error) {
            Alert.alert('Error', error, [
                {
                    text: 'Ok',
                    style: 'destructive',
                },
            ])
        }
    }, [error])

    const deleteRequest = async (id) => {
        setError('')
        setLoading(true)
        try {
            await dispatch(deleteProduct(id))
        } catch (err) {
            setError(err.message)
        }
        setLoading(false)
    }

    const deleteHandler = (id) => {
        Alert.alert('Are you sure?', 'Do you really want to do this?', [
            {text: 'Nope', style: 'default'},
            {
                text: 'Yup',
                style: 'destructive',
                onPress: () => {
                    deleteRequest(id)
                }
            },
        ])
    }

    if (loading) {
        return <View style={styles.centeredContent}>
            <ActivityIndicator size="large" color={Colors.primaryColor}/>
        </View>
    }

    if (!loading && products.length === 0) {
        return <View style={styles.centeredContent}>
            <DefaultText>No data :(</DefaultText>
        </View>
    }

    return (
        <FlatList data={products}
                  onRefresh={loadProducts}
                  refreshing={refreshing}
                  renderItem={itemData =>
                      <ProductItem
                          imageURL={itemData.item.imageURL}
                          title={itemData.item.title}
                          price={itemData.item.price}
                          onSelect={() => {
                              editItemHandler(itemData.item.id)
                          }}
                      >
                          <Button
                              color={Colors.primaryColor}
                              title='Edit'
                              onPress={() => {
                                  editItemHandler(itemData.item.id)
                              }}/>
                          <Button
                              color={Colors.primaryColor}
                              title='Delete'
                              onPress={() => {
                                  deleteHandler(itemData.item.id)
                              }}/>
                      </ProductItem>

                  }
        />
    )
};

const styles = StyleSheet.create({
    centeredContent: {flex: 1, justifyContent: 'center', alignItems: 'center'}
})


UserProductsScreen.navigationOptions = navData => {
    return {
        headerTitle: 'My products',
        headerLeft: () => <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
            <Item
                title='cart'
                iconName='md-menu'
                onPress={() => {
                    navData.navigation.toggleDrawer();
                }}
            />
        </HeaderButtons>
        ,
        headerRight: () => <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
            <Item
                title='Add'
                iconName='md-create'
                onPress={() => {
                    navData.navigation.navigate('EditProduct')
                }}
            />
        </HeaderButtons>
    }
}

export default UserProductsScreen
