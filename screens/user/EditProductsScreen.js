import React, {useCallback, useEffect, useReducer, useState} from 'react';
import {View, ScrollView, StyleSheet, Alert, KeyboardAvoidingView, Button, ActivityIndicator} from "react-native";
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import CustomHeaderButton from "../../components/UI/HeaderButton";
import {useDispatch, useSelector} from "react-redux";
import {createProduct, updateProduct} from "../../redux/actions/products";
import Input from "../../components/UI/Input";
import DefaultText from "../../components/UI/DefaultText";
import Colors from "../../constants/Colors";

const UPDATE_FORM = 'UPDATE_FORM'

const formReducer = (state, action) => {
    switch (action.type) {
        case UPDATE_FORM:
            const updatedValues = {
                ...state.inputValues,
                [action.field]: action.value
            }

            const updatedValidity = {
                ...state.inputsValidity,
                [action.field]: action.valid
            }

            let updatedFormValid = true;

            for (const key in updatedValidity) {
                updatedFormValid = updatedFormValid && updatedValidity[key];
            }

            return {
                ...state,
                inputValues: updatedValues,
                inputsValidity: updatedValidity,
                formValid: updatedFormValid
            }

    }
    return state;
}

const EditProductScreen = props => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');
    const dispatch = useDispatch()
    const productID = props.navigation.getParam('productID');
    const productData = useSelector(state => state.products.myProducts.find(prod => prod.id === productID))

    // Form state
    const [formState, formDispatch] = useReducer(formReducer, {
        inputValues: {
            title: productData ? productData.title : '',
            imageURL: productData ? productData.imageURL : '',
            description: productData ? productData.description : '',
            price: ''
        },
        inputsValidity: {
            title: !!productData,
            imageURL: !!productData,
            description: !!productData,
            price: !!productData,
        },
        formValid: !!productData,
    })

    const submitHandler = useCallback(async () => {
        if (formState.formValid) {
            setError('')
            setLoading(true)

            try {
                if (productData) {
                    await dispatch(updateProduct(
                        productID,
                        formState.inputValues.title,
                        formState.inputValues.description,
                        formState.inputValues.imageURL
                    ))
                } else {
                    await dispatch(createProduct(
                        formState.inputValues.title,
                        formState.inputValues.description,
                        formState.inputValues.imageURL,
                        +formState.inputValues.price
                    ))
                }
                props.navigation.pop()
            } catch (err) {
                setError(err.message)
            }
            setLoading(false)
        } else {
            Alert.alert('Error', 'The form is invalid', [
                {
                    text: 'Ok',
                    style: 'destructive',
                },
            ])
        }

    }, [dispatch, productID, formState])

    useEffect(() => {
        props.navigation.setParams({submit: submitHandler})
    }, [submitHandler])

    useEffect(() => {
        if (error) {
            Alert.alert('Error', error, [
                {
                    text: 'Ok',
                    style: 'destructive',
                },
            ])
        }
    }, [error])

    const onInputUpdate = useCallback((field, value, valid) => {
        formDispatch({
            type: UPDATE_FORM,
            value,
            valid,
            field
        });
    }, [formDispatch])

    // Displays a spinner
    if (loading) {
        return <View style={styles.centeredContent}>
            <ActivityIndicator size="large" color={Colors.primaryColor}/>
        </View>
    }

    return (
        <KeyboardAvoidingView
        >
            <ScrollView>
                <View style={styles.form}>
                    <Input
                        id='title'
                        label='Title'
                        initialValue={formState.inputValues.title}
                        initialValidity={formState.inputsValidity.title}
                        autoCapitalize='sentences'
                        autoCorrect
                        returnKeyType='next'
                        errorMessage='Title is required'
                        onInputUpdate={onInputUpdate}
                        required
                    />
                    <Input
                        id='imageURL'
                        label='Image URL'
                        initialValue={formState.inputValues.imageURL}
                        initialValidity={formState.inputsValidity.imageURL}
                        returnKeyType='next'
                        errorMessage='Image URL is required'
                        onInputUpdate={onInputUpdate}
                        required
                    />
                    <Input
                        id='description'
                        label='Description'
                        initialValue={formState.inputValues.description}
                        initialValidity={formState.inputsValidity.description}
                        returnKeyType='next'
                        errorMessage='Description is required'
                        multiline
                        numberOfLines={3}
                        onInputUpdate={onInputUpdate}
                        required
                    />
                    {productData ? null : (
                        <Input
                            id='price'
                            label='Price'
                            initialValue={formState.inputValues.price}
                            initialValidity={formState.inputsValidity.price}
                            errorMessage='Price is required'
                            keyboardType='decimal-pad'
                            onInputUpdate={onInputUpdate}
                            required
                        />
                    )}
                </View>
            </ScrollView>
        </KeyboardAvoidingView>
    )
}

EditProductScreen.navigationOptions = navData => {
    const submitFunction = navData.navigation.getParam('submit')
    return {
        headerTitle: navData.navigation.getParam('productID') ? 'Edit product' : 'Add product',
        headerRight: () => <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
            <Item
                title='Save'
                iconName='md-checkmark'
                onPress={submitFunction}
            />
        </HeaderButtons>
    }
}

const styles = StyleSheet.create({
    centeredContent: {flex: 1, justifyContent: 'center', alignItems: 'center'},
    form: {
        margin: 20
    },
})

export default EditProductScreen
