import React, {useCallback, useEffect, useReducer, useState} from 'react';
import {
    ScrollView,
    KeyboardAvoidingView,
    View,
    StyleSheet,
    Button,
    Platform,
    ActivityIndicator, Alert
} from "react-native";
import Input from "../../components/UI/Input";
import Card from "../../components/UI/Card";
import Colors from "../../constants/Colors";
import {LinearGradient} from "expo-linear-gradient";
import {useDispatch, useSelector} from "react-redux";
import {register, login} from "../../redux/actions/auth";

const UPDATE_FORM = 'UPDATE_FORM'

const formReducer = (state, action) => {
    switch (action.type) {
        case UPDATE_FORM:
            const updatedValues = {
                ...state.inputValues,
                [action.field]: action.value
            }

            const updatedValidity = {
                ...state.inputsValidity,
                [action.field]: action.valid
            }

            let updatedFormValid = true;

            for (const key in updatedValidity) {
                updatedFormValid = updatedFormValid && updatedValidity[key];
            }

            return {
                ...state,
                inputValues: updatedValues,
                inputsValidity: updatedValidity,
                formValid: updatedFormValid
            }

    }
    return state;
}

const AuthScreen = props => {
    const dispatch = useDispatch()
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');
    const [registerMode, setRegisterMode] = useState(true)

    // Form state
    const [formState, formDispatch] = useReducer(formReducer, {
        inputValues: {
            email: 'richard@branded.com',
            password: '123456'
        },
        inputsValidity: {
            email: false,
            password: false
        },
        formValid: false
    })

    const onInputUpdate = useCallback((field, value, valid) => {
        formDispatch({
            type: UPDATE_FORM,
            value,
            valid,
            field
        });
    }, [formDispatch])

    useEffect(() => {
        if (error) {
            Alert.alert('Error', error, [
                {
                    text: 'Ok',
                    style: 'destructive',
                },
            ])
            setError('')
        }
    }, [error])

    const authHandler = async () => {
        if (formState.formValid) {
            setError('');
            let action;
            if (registerMode) {
                action = register(formState.inputValues.email, formState.inputValues.password)
            } else {
                action = login(formState.inputValues.email, formState.inputValues.password)
            }
            setLoading(true)
            try {
                await dispatch(action)
                props.navigation.navigate('Shop')
            } catch (err) {
                setError(err.message)
                setLoading(false)
            }
        } else {
            Alert.alert('Error', 'The form is not valid', [
                {
                    text: 'Ok',
                    style: 'destructive',
                },
            ])
        }

    }

    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : null}
            style={styles.screen}
        >
            <View style={styles.container}>
                <Card style={styles.card}>
                    <ScrollView>
                        <Input
                            id="email"
                            label="Email"
                            initialValue={formState.inputValues.email}
                            value={formState.inputValues.email}
                            keyboardType="email-address"
                            required
                            email
                            autoCapitalize="none"
                            errorMessage="Please enter a valid email address"
                            onInputUpdate={onInputUpdate}
                        />
                        <Input
                            id="password"
                            label="Password"
                            initialValue={formState.inputValues.password}
                            value={formState.inputValues.password}
                            keyboardType="default"
                            secureTextEntry
                            required
                            email
                            autoCapitalize="none"
                            errorMessage="Please enter a valid password"
                            onInputUpdate={onInputUpdate}
                        />
                        {loading &&
                        <ActivityIndicator size="large" color={Colors.primaryColor}/>
                        }
                        <View style={styles.buttonWrapper}>
                            <Button title={registerMode ? 'Register' : 'Login'} onPress={authHandler}
                                    color={Colors.primaryColor}/>

                        </View>
                        <View style={styles.buttonWrapper}>
                            <Button
                                title={registerMode ? "Switch to login" : "Switch to register"}
                                onPress={() => {
                                    setRegisterMode(!registerMode)
                                }}
                                color={Colors.primaryColor}
                            />
                        </View>

                    </ScrollView>

                </Card>
            </View>
        </KeyboardAvoidingView>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        width: '80%',
        maxWidth: 400,
        maxHeight: 400,
        padding: 20
    },
    buttonWrapper: {
        marginTop: 20,
    }
})

AuthScreen.navigationOptions = navData => {
    return {
        headerTitle: 'Authentication screen',
    }
}


export default AuthScreen
