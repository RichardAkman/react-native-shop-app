import React, {useEffect} from 'react';
import {View, StyleSheet, ActivityIndicator} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import Colors from "../constants/Colors";
import {useDispatch} from "react-redux";
import {authenticate} from "../redux/actions/auth";

const StartupScreen = props => {
    const dispatch = useDispatch()

    useEffect(() => {
        const attemptLogin = async () => {

            try {
                const userData = await AsyncStorage.getItem('userData');

                if (!userData) {
                    props.navigation.navigate('Auth')
                    return;
                }

                const parsedData = JSON.parse(userData)

                const {token, userID, expirationDate} = parsedData;
                const formattedExpirationDate = new Date(expirationDate)

                if (formattedExpirationDate < new Date() || !token || !userID) {
                    props.navigation.navigate('Auth')
                    return;

                }

                props.navigation.navigate('Shop')

                const expirationTime = formattedExpirationDate.getTime() - new Date().getTime()

                dispatch(authenticate(userID, token, expirationTime))
            } catch (err) {
                console.log(err);
            }
        }

        attemptLogin()
    }, [])

    return (
        <View style={styles.screen}>
            <ActivityIndicator
                size={"large"}
                color={Colors.primaryColor}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default StartupScreen
