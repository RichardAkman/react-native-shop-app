import React, {useEffect, useState} from 'react';
import {FlatList, View, StyleSheet, Button, ActivityIndicator, Alert} from "react-native";
import DefaultText from "../../components/UI/DefaultText";
import {useSelector, useDispatch} from "react-redux";
import Colors from "../../constants/Colors";
import TextBold from "../../components/UI/TextBold";
import CartItem from '../../components/shop/CartItem'
import * as cartActions from '../../redux/actions/cart'
import * as orderActions from '../../redux/actions/orders'
import Card from "../../components/UI/Card";

const CartScreen = props => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');
    const totalPrice = useSelector(state => state.cart.totalPrice);
    const cartItems = useSelector(state => {
        let arrayCartItems = [];

        for (const key in state.cart.items) {
            arrayCartItems.push({
                productID: key,
                productTitle: state.cart.items[key].productTitle,
                productPrice: state.cart.items[key].productPrice,
                quantity: state.cart.items[key].quantity,
                sum: state.cart.items[key].sum,
            })
        }

        arrayCartItems.sort((a, b) => a.productID > b.productID ? 1 : -1)

        return arrayCartItems;
    });

    const dispatch = useDispatch();

    const sendOrderHandler = async () => {
        setError('')
        setLoading(true)
        try {
            await dispatch(orderActions.addOrder(cartItems, totalPrice))
        } catch (err) {
            setError(err.message)
        }
        setLoading(false)
    }

    useEffect(() => {
        if (error) {
            Alert.alert('Error', error, [
                {
                    text: 'Ok',
                    style: 'destructive',
                },
            ])
        }
    }, [error])

    return (
        <View style={styles.screenContainer}>
            <Card style={styles.summaryContainer}>
                <TextBold style={styles.totalPrice}>Total price: <DefaultText
                    style={styles.innerPrice}>${totalPrice.toFixed(2)}</DefaultText></TextBold>
                {loading ? (
                    <ActivityIndicator size="large" color='white'/>
                ) : (
                    <Button title='Order'
                            color={Colors.primaryColor}
                            disabled={cartItems.length === 0}
                            onPress={sendOrderHandler}/>
                )}
            </Card>
            <FlatList data={cartItems}
                      keyExtractor={item => item.productID}
                      renderItem={
                          itemData =>
                              <CartItem
                                  title={itemData.item.productTitle}
                                  quantity={itemData.item.quantity}
                                  sum={itemData.item.sum}
                                  deletable
                                  onRemove={() => {
                                      dispatch(cartActions.removeFromCart(itemData.item.productID))
                                  }}
                              />
                      }/>
        </View>
    )
}

CartScreen.navigationOptions = {
    headerTitle: 'Your cart'
}

const styles = StyleSheet.create({
    screenContainer: {
        margin: 20
    },
    summaryContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        margin: 10,
        paddingLeft: 10,
    },
    totalPrice: {
        fontSize: 14,
    },
    innerPrice: {
        color: 'grey',
    },
})

export default CartScreen
