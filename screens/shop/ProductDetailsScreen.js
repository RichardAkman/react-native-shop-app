import React from "react";
import {View, StyleSheet, Image, Button, ScrollView} from "react-native";
import {useSelector, useDispatch} from "react-redux";
import Colors from "../../constants/Colors";
import DefaultText from "../../components/UI/DefaultText";
import TextBold from "../../components/UI/TextBold";
import * as cartActions from "../../redux/actions/cart";

const ProductDetailsScreen = props => {
    const productID = props.navigation.getParam('productID');
    const product = useSelector(state => state.products.availableProducts.find(prod => prod.id === productID))
    const dispatch = useDispatch()

    return (
        <ScrollView>
            <Image
                style={styles.image}
                source={{uri: product.imageURL}}
            />
            <View style={styles.buttonContainer}>
                <Button color={Colors.primaryColor} title='Add to cart' onPress={() => {
                    dispatch(cartActions.addToCart(product))
                }}/>
            </View>
            <TextBold style={styles.price}>${product.price}</TextBold>
            <DefaultText style={styles.description}>{product.description}</DefaultText>
        </ScrollView>
    )
}

ProductDetailsScreen.navigationOptions = navData => {
    return {
        headerTitle: navData.navigation.getParam('productTitle')
    }
}

const styles = StyleSheet.create({
    productContainer: {},
    imageWrapper: {
        width: '100%',
        height: '50%',
        overflow: 'hidden',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    image: {
        width: '100%',
        height: 200
    },
    details: {
        alignItems: 'center',
        height: '25%',
        padding: 10
    },
    title: {
        fontSize: 18,
        marginVertical: 5
    },
    price: {
        fontSize: 14,
        color: 'grey',
        marginVertical: 5,
        textAlign: 'center'
    },
    description: {
        fontSize: 16,
        textAlign: 'center',
        marginHorizontal: 20,
    },
    buttonContainer: {
        alignItems: 'center',
        marginVertical: 10
    }

})

export default ProductDetailsScreen
