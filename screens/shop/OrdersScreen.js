import React, {useCallback, useEffect, useState} from 'react';
import {ActivityIndicator, Button, FlatList, StyleSheet, Text, View} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import CustomHeaderButton from "../../components/UI/HeaderButton";
import OrderItem from "../../components/shop/OrderItem";
import DefaultText from "../../components/UI/DefaultText";
import Colors from "../../constants/Colors";
import {getOrders} from "../../redux/actions/orders";

const OrdersScreen = props => {
    const [loading, setLoading] = useState(false)
    const [refreshing, setRefreshing] = useState(false);
    const [error, setError] = useState('');
    const dispatch = useDispatch();
    const orders = useSelector(state => state.orders.orders);

    // Setup order loading from the server
    const loadOrders = useCallback(async () => {
        setError('');
        setRefreshing(true)
        try {
            await dispatch(getOrders())
        } catch (err) {
            setError(err.message);
        }
        setRefreshing(false)
    }, [dispatch, setLoading, setError])

    // Initial fetch of data, will be only activated once.
    useEffect(() => {
        setLoading(true);
        loadOrders().then(() => {
            setLoading(false)
        });
    }, [dispatch, loadOrders, setLoading])

    // Upon re-entering the screen, fetch the latest data in case a change was made
    useEffect(() => {
        const willFocusSubscription = props.navigation.addListener('willFocus', loadOrders)

        return () => {
            willFocusSubscription.remove();
        }
    }, [loadOrders])

    // In case an error from the server occurs
    if (error) {
        return <View style={styles.centeredContent}>
            <DefaultText>An error occurred</DefaultText>
            <Button title="try again" onPress={loadOrders} color={Colors.primaryColor}/>
        </View>
    }

    if (loading) {
        return <View style={styles.centeredContent}>
            <ActivityIndicator size="large" color={Colors.primaryColor}/>
        </View>
    }

    if (!loading && orders.length === 0) {
        return <View style={styles.centeredContent}>
            <DefaultText>No data :(</DefaultText>
        </View>
    }

    return (
        <FlatList
            onRefresh={loadOrders}
            refreshing={refreshing}
            data={orders} renderItem={
            itemData =>
                <OrderItem
                    totalPrice={itemData.item.totalPrice}
                    date={itemData.item.readableDate}
                    items={itemData.item.items}
                />
        }
        />
    )
};

const styles = StyleSheet.create({
    centeredContent: {flex: 1, justifyContent: 'center', alignItems: 'center'}
})

OrdersScreen.navigationOptions = navData => {
    return {
        headerTitle: 'Your orders',
        headerLeft: () => <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
            <Item
                title='cart'
                iconName='md-menu'
                onPress={() => {
                    navData.navigation.toggleDrawer()
                }}
            />
        </HeaderButtons>
    }
}

export default OrdersScreen;
