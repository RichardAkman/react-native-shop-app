import React, {useCallback, useEffect, useState} from 'react';
import {FlatList, Button, ActivityIndicator, View, StyleSheet} from "react-native";
import {useSelector} from "react-redux";
import ProductItem from "../../components/shop/ProductItem";
import {useDispatch} from "react-redux";
import * as cartActions from "../../redux/actions/cart";
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import CustomHeaderButton from "../../components/UI/HeaderButton";
import Colors from "../../constants/Colors";
import {getProducts} from "../../redux/actions/products";
import DefaultText from "../../components/UI/DefaultText";

const ProductsOverviewScreen = props => {
    const [loading, setLoading] = useState(false);
    const [refreshing, setRefreshing] = useState(false);
    const [error, setError] = useState('');
    const products = useSelector(state => state.products.availableProducts)
    const dispatch = useDispatch();

    // Setup product loading from the server
    const loadProducts = useCallback(async () => {
        setError('');
        setRefreshing(true)
        try {
            await dispatch(getProducts())
        } catch (err) {
            setError(err.message);
        }
        setRefreshing(false)
    }, [dispatch, setLoading, setError, setRefreshing])

    // Initial fetch of data, will be only activated once.
    useEffect(() => {
        setLoading(true);
        loadProducts().then(() => {
            setLoading(false)
        });
    }, [dispatch, loadProducts])

    // Upon re-entering the screen, fetch the latest data in case a change was made
    useEffect(() => {
        const willFocusSubscription = props.navigation.addListener('willFocus', loadProducts)

        return () => {
            willFocusSubscription.remove();
        }
    }, [loadProducts])

    // Takes the client to the inner product screen
    const selectItemHandler = (id, title) => {
        props.navigation.navigate('ProductDetails', {
            productID: id,
            productTitle: title,
        });
    }

    // In case an error from the server occurs
    if (error) {
        return <View style={styles.centeredContent}>
            <DefaultText>An error occurred</DefaultText>
            <Button title="try again" onPress={loadProducts} color={Colors.primaryColor}/>
        </View>
    }

    // Displays a spinner
    if (loading) {
        return <View style={styles.centeredContent}>
            <ActivityIndicator size="large" color={Colors.primaryColor}/>
        </View>
    }

    // In case there is no data
    if (!loading && products.length === 0) {
        return <View style={styles.centeredContent}>
            <DefaultText>No data :(</DefaultText>
        </View>
    }

    // The default UI when products are available
    return (
        <FlatList
            onRefresh={loadProducts}
            refreshing={refreshing}
            data={products}
            renderItem={
                itemData =>
                    <ProductItem
                        imageURL={itemData.item.imageURL}
                        title={itemData.item.title}
                        price={itemData.item.price}
                        onSelect={() => {
                            selectItemHandler(itemData.item.id, itemData.item.title)
                        }}
                    >
                        <Button color={Colors.primaryColor} title='View details'
                                onPress={() => {
                                    selectItemHandler(itemData.item.id, itemData.item.title)
                                }}/>
                        <Button color={Colors.primaryColor} title='Add to cart' onPress={() => {
                            dispatch(cartActions.addToCart(itemData.item))
                        }}/>
                    </ProductItem>
            }
        />
    )
}

/**
 * Navigation options of the screen
 *
 * @param navData
 * @returns {{headerRight: (function(): *), headerLeft: (function(): *), headerTitle: string}}
 */
ProductsOverviewScreen.navigationOptions = navData => {
    return {
        headerTitle: 'Products',
        headerRight: () => <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
            <Item
                title='cart'
                iconName='md-cart'
                onPress={() => {
                    navData.navigation.navigate('Cart')
                }}
            />
        </HeaderButtons>,
        headerLeft: () => <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
            <Item
                title='cart'
                iconName='md-menu'
                onPress={() => {
                    navData.navigation.toggleDrawer();
                }}
            />
        </HeaderButtons>
    }
}

const styles = StyleSheet.create({
    centeredContent: {flex: 1, justifyContent: 'center', alignItems: 'center'}
})

export default ProductsOverviewScreen
