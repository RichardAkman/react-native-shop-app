import React from 'react';
import {createStackNavigator} from 'react-navigation-stack'
import {createAppContainer, createSwitchNavigator} from 'react-navigation'
import {createDrawerNavigator, DrawerItems} from 'react-navigation-drawer'
import Colors from "../constants/Colors";
import {Ionicons} from "@expo/vector-icons";
import {SafeAreaView, Platform, Button, View} from "react-native"
import {useDispatch} from "react-redux";

// Screen imports
import ProductsOverviewScreen from "../screens/shop/ProductsOverviewScreen";
import ProductDetailsScreen from "../screens/shop/ProductDetailsScreen";
import CartScreen from "../screens/shop/CartScreen";
import OrdersScreen from "../screens/shop/OrdersScreen";
import UserProductsScreen from "../screens/user/UserProductsScreen";
import EditProductScreen from "../screens/user/EditProductsScreen";
import AuthScreen from "../screens/user/AuthScreen";
import StartupScreen from "../screens/StartupScreen";
import {logout} from "../redux/actions/auth";

const defaultNavigationOptions = {
    headerStyle: {
        backgroundColor: Colors.primaryColor,
    },
    headerTitleStyle: {
        fontFamily: 'open-sans-bold'
    },
    headerBackTitleStyle: {
        fontFamily: 'open-sans'
    },
    headerTintColor: 'white'
}

const ProductsNavigator = createStackNavigator({
    ProductsOverview: ProductsOverviewScreen,
    ProductDetails: ProductDetailsScreen,
    Cart: CartScreen,
}, {
    defaultNavigationOptions: defaultNavigationOptions
});

const OrdersNavigator = createStackNavigator({
    Orders: OrdersScreen
}, {
    defaultNavigationOptions: defaultNavigationOptions
})

const AdminNavigator = createStackNavigator({
    UserProducts: UserProductsScreen,
    EditProduct: EditProductScreen
}, {
    defaultNavigationOptions: defaultNavigationOptions
})

const AppNavigator = createDrawerNavigator({
    Products: {
        screen: ProductsNavigator,
        navigationOptions: {
            drawerIcon: drawerConfig => <Ionicons
                name='md-cart'
                size={20}
                color={drawerConfig.tintColor}
            />
        }
    },
    Orders: {
        screen: OrdersNavigator,
        navigationOptions: {
            drawerIcon: drawerConfig => <Ionicons
                name='md-list'
                size={20}
                color={drawerConfig.tintColor}
            />
        }
    },
    Admin: {
        screen: AdminNavigator,
        navigationOptions: {
            drawerIcon: drawerConfig => <Ionicons
                name='md-create'
                size={20}
                color={drawerConfig.tintColor}
            />
        }
    }
}, {
    contentOptions: {
        activeTintColor: Colors.primaryColor,
        labelStyle: {
            fontFamily: 'open-sans-bold',
        }
    },
    contentComponent: props => {
        const dispatch = useDispatch();

        return (
            <View style={{flex: 1, paddingTop: 20}}>
                <SafeAreaView
                    ForceInset={{top: 'always', horizontal: 'never'}}
                >
                    <DrawerItems {...props}/>
                    <Button title="Logout" color={Colors.primaryColor} onPress={() => {
                        dispatch(logout())
                    }}/>
                </SafeAreaView>
            </View>
        )
    }
})

const AuthNavigator = createStackNavigator({
    Auth: AuthScreen
}, {
    defaultNavigationOptions: defaultNavigationOptions
})

const MainNavigator = createSwitchNavigator({
    Startup: StartupScreen,
    Auth: AuthNavigator,
    Shop: AppNavigator,
})

export default createAppContainer(MainNavigator)
