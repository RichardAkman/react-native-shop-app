import React, {useEffect, useRef} from 'react';
import ShopNavigator from "./ShopNavigator";
import {useSelector} from "react-redux";
import {NavigationActions} from "react-navigation";

const NavigationContainer = props => {
    const navigationRef = useRef();
    const isAuthenticated = useSelector(state => !!state.auth.token)

    useEffect(() => {
        if (!isAuthenticated) {
            navigationRef.current.dispatch(NavigationActions.navigate({routeName: 'Auth'}))
        }
    }, [isAuthenticated])

    return (
        <ShopNavigator ref={navigationRef}/>
    )
}
export default NavigationContainer
