import Config from "../../constants/Config";
import Order from "../../models/order";

export const ADD_ORDER = 'ADD_ORDER';
export const GET_ORDERS = 'GET_ORDERS';

export const addOrder = (cartItems, totalPrice) => async (dispatch, getState) => {
    try {
        const authToken = getState().auth.token;
        const userID = getState().auth.userID;
        const date = new Date().toISOString();
        const body = JSON.stringify({cartItems, totalPrice, date});

        const response = await fetch(`${Config.API_URL}/orders/${userID}.json?auth=${authToken}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: body
        })

        if (!response.ok) {
            throw new Error('something unexpected happened')
        }

        const responseData = await response.json();

        if (responseData.name) {
            dispatch({
                type: ADD_ORDER, data: {
                    id: responseData.name,
                    items: cartItems,
                    totalPrice: totalPrice,
                    date
                }
            });
        }
    } catch (err) {
        // Send somewhere
        throw err;
    }
}

export const getOrders = () => async (dispatch, getState) => {
    const authToken = getState().auth.token;
    const userID = getState().auth.userID;

    const response = await fetch(`${Config.API_URL}/orders/${userID}.json?auth=${authToken}`)

    const responseData = await response.json();

    let loadedData = [];

    for (const key in responseData) {
        loadedData.push(
            new Order(
                key,
                responseData[key].cartItems,
                +responseData[key].totalPrice,
                new Date(responseData[key].date),
            ))
    }

    if (responseData) {
        dispatch({
            type: GET_ORDERS,
            orders: loadedData,
        })
    }
}
