import Config from '../../constants/Config'
import Product from "../../models/product";

export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const CREATE_PRODUCT = 'CREATE_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const GET_PRODUCTS = 'GET_PRODUCTS';

export const deleteProduct = productID => async (dispatch, getState) => {
    const authToken = getState().auth.token;
    const response = await fetch(`${Config.API_URL}/products/${productID}.json?auth=${authToken}`, {
        method: 'DELETE',
    })

    if (!response.ok) {
        throw new Error('An error occurred')
    }

    dispatch({type: DELETE_PRODUCT, productID: productID});
}

export const createProduct = (title, description, imageURL, price) => async (dispatch, getState) => {
    try {
        const authToken = getState().auth.token;
        const userID = getState().auth.userID;

        const response = await fetch(`${Config.API_URL}/products.json?auth=${authToken}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                title, description, imageURL, price, creatorID: userID
            })
        })

        if (!response.ok) {
            throw new Error('something unexpected happened')
        }

        const responseData = await response.json();

        if (responseData.name) {
            dispatch({
                type: CREATE_PRODUCT, productData: {
                    id: responseData.name,
                    title,
                    description,
                    imageURL,
                    price,
                    creatorID: userID
                }
            })
        }
    } catch (err) {
        // Send somewhere
        throw err;
    }
}

export const getProducts = () => async (dispatch, getState) => {
    const authToken = getState().auth.token;
    const userID = getState().auth.userID;

    const response = await fetch(`${Config.API_URL}/products.json?auth=${authToken}`)

    const responseData = await response.json();

    let loadedData = [];

    for (const key in responseData) {
        loadedData.push(
            new Product(
                key,
                responseData[key].creatorID,
                responseData[key].title,
                responseData[key].imageURL,
                responseData[key].description,
                +responseData[key].price
            ))
    }

    if (responseData) {
        dispatch({
            type: GET_PRODUCTS,
            products: loadedData,
            createdProducts: loadedData.filter(prod => prod.creatorID === userID)
        })
    }
}

export const updateProduct = (id, title, description, imageURL) => async (dispatch, getState) => {
    const authToken = getState().auth.token;

    const response = await fetch(`${Config.API_URL}/products/${id}.json?auth=${authToken}`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({title, description, imageURL})
    })

    if (!response.ok) {
        throw new Error('An error occurred')
    }

    dispatch({
        type: UPDATE_PRODUCT, productID: id, productData: {
            title,
            description,
            imageURL
        }
    })
}
