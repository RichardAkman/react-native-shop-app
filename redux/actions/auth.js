import Config from "../../constants/Config";
import AsyncStorage from "@react-native-community/async-storage";

export const LOGOUT = 'LOGOUT';
export const AUTHENTICATE = 'AUTHENTICATE';

export const authenticate = (userID, token, expirationTimer) => async (dispatch) => {
    dispatch(setLogoutTimer(expirationTimer))
    dispatch({type: AUTHENTICATE, userID, token})
}

let timer;

export const register = (email, password) => async (dispatch) => {
    const response = await fetch(`${Config.FIREBASE_AUTH_API_URL}signUp?key=${Config.FIREBASE_WEB_API_KEY}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email, password, returnSecureToken: true
        })
    })

    const responseData = await response.json();

    if (!response.ok) {
        let msg = 'something unexpected happened'

        const errorID = responseData.error.message;

        console.log(errorID)

        switch (errorID) {
            case 'EMAIL_EXISTS':
                msg = 'This email is already taken'
                break;
        }

        throw new Error(msg)
    }

    dispatch(authenticate(responseData.localId, responseData.idToken, parseInt(responseData.expiresIn) * 1000))
}

export const login = (email, password) => async (dispatch) => {
    console.log('test')
    const response = await fetch(`${Config.FIREBASE_AUTH_API_URL}signInWithPassword?key=${Config.FIREBASE_WEB_API_KEY}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email, password, returnSecureToken: true
        })
    })

    const responseData = await response.json();

    if (!response.ok) {
        let msg = 'something unexpected happened'

        console.log(responseData)

        const errorID = responseData.error.message;

        switch (errorID) {
            case 'EMAIL_NOT_FOUND':
                msg = 'This email was not found'
                break;
            case 'INVALID_PASSWORD':
                msg = 'The credentials provided are incorrect'
                break;
        }

        console.log(msg)

        throw new Error(msg)
    }

    const expirationDate = new Date(new Date().getTime() + parseInt(responseData.expiresIn) * 1000).toISOString()

    dispatch(authenticate(responseData.localId, responseData.idToken, parseInt(responseData.expiresIn) * 1000))

    saveToken(responseData.idToken, responseData.localId, expirationDate)
}

export const logout = () => {
    clearTimer()
    AsyncStorage.removeItem('userData')
    return ({type: LOGOUT})
}

const saveToken = (token, userID, expirationDate) => {
    AsyncStorage.setItem('userData', JSON.stringify({token, userID, expirationDate}));
}

const clearTimer = () => {
    if (timer)
        clearTimeout(timer)
}

const setLogoutTimer = (expirationTime) => async (dispatch) => {
    timer = setTimeout(() => {
        dispatch(logout())
    }, expirationTime)
}
