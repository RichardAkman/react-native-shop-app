import {AUTHENTICATE, LOGOUT} from "../actions/auth";

const initialState = {
    token: null,
    userID: null
}

export default (state = initialState, action) => {
    switch (action.type) {
        case AUTHENTICATE:
            return {
                ...state,
                token: action.token,
                userID: action.userID,
            }
        case LOGOUT:
            return initialState
    }
    return state
}
