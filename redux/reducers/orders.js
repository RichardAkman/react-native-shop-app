import {ADD_ORDER, GET_ORDERS} from "../actions/orders";
import Order from "../../models/order";

const initialState = {
    orders: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_ORDERS:
            return {
                ...state,
                orders: action.orders
            }
        case ADD_ORDER:
            const newOrder = new Order(action.data.id, action.data.items, action.data.totalPrice, action.data.date)
            return {
                ...state,
                orders: [...state.orders, newOrder]
                // orders: state.orders.concat(newOrder)
            }
    }
    return state
}
