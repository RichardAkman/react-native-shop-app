import {DELETE_PRODUCT, CREATE_PRODUCT, UPDATE_PRODUCT, GET_PRODUCTS} from "../actions/products";
import Product from "../../models/product";

const initialState = {
    availableProducts: [],
    myProducts: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_PRODUCTS:
            return {
                ...state,
                availableProducts: action.products,
                myProducts: action.createdProducts
            }
        case DELETE_PRODUCT:
            return {
                ...state,
                myProducts: state.myProducts.filter(product => product.id !== action.productID),
                availableProducts: state.availableProducts.filter(product => product.id !== action.productID)
            }
        case CREATE_PRODUCT:
            const product = new Product(
                action.productData.id,
                action.productData.creatorID,
                action.productData.title,
                action.productData.imageURL,
                action.productData.description,
                action.productData.price
            )
            return {
                ...state,
                myProducts: [...state.myProducts, product],
                availableProducts: [...state.availableProducts, product]
            }
        case UPDATE_PRODUCT:
            const productIndex = state.myProducts.findIndex(prod => prod.id === action.productID)
            const updatedProduct = new Product(
                action.productID,
                state.myProducts[productIndex].creatorID,
                action.productData.title,
                action.productData.imageURL,
                action.productData.description,
                state.myProducts[productIndex].price
            )
            const updatedMyProducts = [...state.myProducts];

            const availableProductIndex = state.availableProducts.findIndex(prod => prod.id === action.productID)
            const updatedAvailableProducts = [...state.availableProducts];

            updatedAvailableProducts[availableProductIndex] = updatedProduct
            updatedMyProducts[productIndex] = updatedProduct

            return {
                ...state,
                myProducts: updatedMyProducts,
                availableProducts: updatedAvailableProducts
            }
    }
    return state
}
