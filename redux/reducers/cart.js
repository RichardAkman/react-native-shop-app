import {ADD_TO_CART, REMOVE_FROM_CART} from "../actions/cart";
import CartItem from "../../models/cart-item";
import {ADD_ORDER} from "../actions/orders";
import {DELETE_PRODUCT} from "../actions/products";

const initialState = {
    items: {},
    totalPrice: 0
}

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_CART:
            const addedProduct = action.product;
            const productPrice = addedProduct.price;
            const productTitle = addedProduct.title;
            let updatedOrNewCartItem;

            if (state.items[addedProduct.id]) {
                updatedOrNewCartItem = new CartItem(state.items[addedProduct.id].quantity + 1,
                    productPrice,
                    productTitle,
                    state.items[addedProduct.id].sum + productPrice);
            } else {
                updatedOrNewCartItem = new CartItem(1, productPrice, productTitle, productPrice);
            }

            return {
                ...state,
                items: {
                    ...state.items,
                    [addedProduct.id]: updatedOrNewCartItem
                },
                totalPrice: state.totalPrice + updatedOrNewCartItem.productPrice
            }
        case REMOVE_FROM_CART:
            const selectedItem = state.items[action.productID]
            const currentQuantity = selectedItem.quantity;
            let updatedCartItems = {...state.items};

            if (currentQuantity > 1) {
                const updatedItem = new CartItem(selectedItem.quantity - 1,
                    selectedItem.productPrice,
                    selectedItem.productTitle,
                    selectedItem.sum - selectedItem.productPrice)

                updatedCartItems = {
                    ...state.items,
                    [action.productID]: updatedItem
                }
            } else {
                delete updatedCartItems[action.productID];
            }

            return {
                ...state,
                items: updatedCartItems,
                totalPrice: state.totalPrice - selectedItem.productPrice
            }
        case ADD_ORDER:
            return initialState;
        case DELETE_PRODUCT:
            if (!state.items[action.productID]) {
                return state;
            }

            const updatedItems = {...state.items}
            const itemPrice = state.items[action.productID].sum
            delete updatedItems[action.productID]
            return {
                ...state,
                items: updatedItems,
                totalPrice: state.totalPrice - itemPrice
            }
    }
    return state
}
