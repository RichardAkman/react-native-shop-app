import React, {useState} from 'react';
import * as Font from 'expo-font'
import {AppLoading} from "expo";
import {createStore, combineReducers, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import productsReducer from './redux/reducers/products'
import cartReducer from './redux/reducers/cart'
import ordersReducer from './redux/reducers/orders'
import authReducer from './redux/reducers/auth'
import {composeWithDevTools} from "redux-devtools-extension";
import ReduxThunk from 'redux-thunk'
import NavigationContainer from "./navigation/NavigationContainer";

const rootReducer = combineReducers({
    products: productsReducer,
    cart: cartReducer,
    orders: ordersReducer,
    auth: authReducer,
})

const store = createStore(rootReducer, applyMiddleware(ReduxThunk))
// TODO - Remove before deployment
// const store = createStore(rootReducer,applyMiddleware(ReduxThunk),composeWithDevTools())

const fetchFonts = () => {
    return Font.loadAsync({
        'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
        'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
    })
}

export default function App() {
    const [fontLoaded, setFontLoaded] = useState(false);

    if (!fontLoaded) {
        return <AppLoading startAsync={fetchFonts} onFinish={() => setFontLoaded(true)}/>
    }

    return (
        <Provider store={store}>
            <NavigationContainer/>
        </Provider>
    );
}
