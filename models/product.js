class Product {
    constructor(id, creatorID, title, imageURL, description, price) {
        this.id = id
        this.creatorID = creatorID
        this.title = title
        this.imageURL = imageURL
        this.description = description
        this.price = price
    }
}

export default Product
