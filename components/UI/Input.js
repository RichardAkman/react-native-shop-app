import React, {useReducer, useEffect} from 'react';
import {View, TextInput, StyleSheet} from "react-native";
import DefaultText from "./DefaultText";
import validator from "../validator";

const UPDATE_INPUT = 'UPDATE_INPUT'
const INPUT_BLUR = 'INPUT_BLUR'

const inputReducer = (state, action) => {
    switch (action.type) {
        case UPDATE_INPUT:
            return {
                ...state,
                value: action.value,
                valid: action.valid,
            }
        case INPUT_BLUR:
            return {
                ...state,
                touched: true
            }
    }
    return state;
}

const Input = props => {
    const [inputState, inputDispatch] = useReducer(inputReducer, {
        value: props.initialValue ? props.initialValue : '',
        valid: props.initialValidity ? props.initialValidity : false,
        touched: false
    })

    const {onInputUpdate, id} = props

    useEffect(() => {
        props.onInputUpdate(
            id,
            inputState.value,
            inputState.valid
        )
    }, [inputState, onInputUpdate, id])

    const textChangeHandler = (text) => {
        let isValid = true;

        if (props.required && text.trim().length === 0) {
            isValid = false;
        }

        if (props.email) {
            validator.emailValidator(text)
        }

        inputDispatch({
            type: UPDATE_INPUT,
            value: text,
            valid: isValid
        })
    }

    const focusLostHandler = () => {
        inputDispatch({
            type: INPUT_BLUR
        })
    }

    return (
        <View>
            <View style={styles.formControl}>
                <DefaultText style={styles.label}>{props.label}</DefaultText>
                <TextInput
                    {...props}
                    style={styles.input}
                    value={inputState.value}
                    onChangeText={textChangeHandler}
                    onBlur={focusLostHandler}
                />
            </View>
            {!inputState.valid && inputState.touched &&
            <DefaultText style={styles.errorText}>{props.errorMessage}</DefaultText>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    formControl: {
        width: '100%'
    },
    label: {
        marginVertical: 5
    },
    input: {
        paddingVertical: 5,
        paddingHorizontal: 5,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1
    },
    errorText: {
        color: 'red',
        fontSize: 12
    }
})

export default Input
