import React from 'react';
import {View, StyleSheet, Image, TouchableOpacity, TouchableNativeFeedback, Platform} from "react-native";
import DefaultText from "../UI/DefaultText";
import TextBold from "../UI/TextBold";
import Card from "../UI/Card";

const ProductItem = props => {
    let TouchableComponent = TouchableOpacity;

    if (Platform.OS === 'android' && Platform.Version >= 21) {
        TouchableComponent = TouchableNativeFeedback;
    }

    return (
        <Card style={styles.productContainer}>
            <View style={styles.touchableWrapper}>
                <TouchableComponent useForeground onPress={props.onSelect}>
                    <View>
                        <View style={styles.imageWrapper}>
                            <Image
                                style={styles.image}
                                source={{uri: props.imageURL}}
                                resizeMode='contain'
                            />
                        </View>
                        <View style={styles.details}>
                            <TextBold style={styles.title}>{props.title}</TextBold>
                            <DefaultText style={styles.price}>${props.price.toFixed(2)}</DefaultText>
                        </View>
                        <View style={styles.buttonsContainer}>
                            {props.children}
                        </View>
                    </View>
                </TouchableComponent>
            </View>
        </Card>
    )

};

const styles = StyleSheet.create({
    productContainer: {
        height: 250,
        margin: 20,
        overflow: 'hidden'
    },
    touchableWrapper: {
        borderRadius: 10,
        overflow: 'hidden'
    },
    imageWrapper: {
        width: '100%',
        height: '50%',
        overflow: 'hidden',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    image: {
        width: '100%',
        height: '100%'
    },
    details: {
        alignItems: 'center',
        height: '25%',
        padding: 10
    },
    title: {
        fontSize: 18,
        marginVertical: 5
    },
    price: {
        fontSize: 14,
        color: 'grey'
    },
    buttonsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: '25%',
        paddingHorizontal: 10
    }

})

export default ProductItem
