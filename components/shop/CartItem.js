import React from 'react';
import {View, StyleSheet, Button, TouchableOpacity, Platform, TouchableNativeFeedback} from "react-native";
import Colors from "../../constants/Colors";
import DefaultText from "../UI/DefaultText";
import TextBold from "../UI/TextBold";
import {Ionicons} from '@expo/vector-icons'

const CartItem = props => {
    let TouchableComponent = TouchableOpacity;

    if (Platform.OS === 'android' && Platform.Version >= 21) {
        TouchableComponent = TouchableNativeFeedback;
    }

    return (
        <View style={styles.itemContainer}>
            <View>
                <View style={styles.details}>
                    <TextBold style={styles.title}>{props.title}</TextBold>
                    <DefaultText style={styles.quantity}>Quantity: {props.quantity}</DefaultText>
                    <DefaultText style={styles.sum}>Price: ${props.sum}</DefaultText>
                    {props.deletable && <TouchableComponent
                        onPress={props.onRemove}
                    >
                        <Ionicons
                            name='md-trash'
                            size={20}
                            color='red'
                        />
                    </TouchableComponent>}
                </View>
            </View>
        </View>
    )

};

const styles = StyleSheet.create({
    itemContainer: {
        shadowColor: 'black',
        shadowOpacity: 0.5,
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 6,
        elevation: 5,
        borderRadius: 10,
        backgroundColor: 'white',
        paddingVertical: 10,
        margin: 20,
        overflow: 'hidden'
    },
    touchableWrapper: {
        borderRadius: 10,
        overflow: 'hidden'
    },
    details: {
        alignItems: 'center',
    },
    title: {
        fontSize: 18,
        marginVertical: 5
    },
    sum: {
        marginVertical: 5
    },
})

export default CartItem
