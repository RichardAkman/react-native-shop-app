import React, {useState} from 'react';
import {View, StyleSheet, Button} from "react-native";
import DefaultText from "../UI/DefaultText";
import TextBold from "../UI/TextBold";
import Colors from "../../constants/Colors";
import CartItem from "./CartItem";
import * as cartActions from "../../redux/actions/cart";
import Card from "../UI/Card";

const OrderItem = props => {
    const [showDetails, setShowDetails] = useState(false);

    return (
        <Card style={styles.orderContainer}>
            <View style={styles.details}>
                <TextBold style={styles.totalPrice}>${props.totalPrice.toFixed(2)}</TextBold>
                <DefaultText style={styles.date}>{props.date}</DefaultText>
            </View>
            <View style={styles.buttonContainer}>
                <Button color={Colors.primaryColor}
                        title={showDetails ? 'hide' : 'show details'}
                        onPress={() => {
                            setShowDetails(prevState => !prevState)
                        }}/>
            </View>
            {showDetails && <View>
                {props.items.map(cartItem => {
                    return (
                        <CartItem
                            key={cartItem.productID}
                            title={cartItem.productTitle}
                            quantity={cartItem.quantity}
                            sum={cartItem.sum}
                        />
                    )
                })}
            </View>}
        </Card>
    )

};

const styles = StyleSheet.create({
    orderContainer: {
        margin: 20,
        padding: 10,
    },
    details: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    totalPrice: {},
    date: {
        color: 'grey'
    },
    buttonContainer: {
        alignItems: 'center',
        marginTop: 10
    }
})

export default OrderItem
